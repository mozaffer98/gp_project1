import 'package:conditional_builder/conditional_builder.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:gp/Network/DioUtil.dart';
import 'package:gp/homelayout/homelayout.dart';
import 'package:gp/models/User.dart';
import 'package:gp/modules/DisableHomeScreen/homeScreendisable.dart';
import 'package:gp/modules/Login/cubit/logincubit.dart';
import 'package:gp/modules/Login/cubit/loginstatuts.dart';
import 'package:gp/modules/Siginup/Sginup%20Screen.dart';
import 'package:gp/modules/VoultneerHome/voultneerhome.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  bool obscureText = false;
  final emailController = TextEditingController();
  final passwordController = TextEditingController();
  final formKey = GlobalKey<FormState>();
  bool isloading = false;
  bool _obscureText = true;

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => LoginCubit(),
      child: BlocConsumer<LoginCubit, LoginStates>(
        listener: (context, state) {
          if (state is LoginSuccessState) {
            if (state.loginModel.status == 'Success') {
              Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => const  HomeScreenDisable()),
              );
            }
          }
        },
        builder: (context, status) {
          return Scaffold(
            key: scaffoldKey,
            appBar: null,
            body: Padding(
              padding: EdgeInsets.all(20.0),
              child: Center(
                child: SingleChildScrollView(
                  child: Form(
                    key: formKey, //دا اللى هيشغلى اللى  validator
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text(
                          'Sign In ',
                          style: TextStyle(
                            fontSize: 40.0,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        const SizedBox(
                          height: 16.0,
                        ),
                        // Text('  any thing   ',
                        //     style: Theme.of (context).textTheme.bodyText1!.copyWith(
                        //       color: Colors.grey, fontSize: 24),

                        //   ),
                        const SizedBox(
                          height: 40.0,
                        ),
                        TextFormField(
                          controller: emailController,
                          keyboardType: TextInputType.emailAddress,
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return 'Please enter your email';
                            }
                            return null;
                          },
                          decoration: const InputDecoration(
                            labelText: 'Email Address',
                            prefixIcon: Icon(
                              Icons.email,
                            ),
                            border: OutlineInputBorder(),
                          ),
                        ),
                        const SizedBox(
                          height: 10.0,
                        ),
                        TextFormField(
                          controller: passwordController,
                          keyboardType: TextInputType.visiblePassword,
                          obscureText: _obscureText,
                          // عشان الباسورد ميظهرش
                          onChanged: (String value) {},
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return 'Please enter your  password';
                            }
                            return null;
                          },

                          decoration: InputDecoration(
                              labelText: 'Password',
                              border: OutlineInputBorder(),
                              prefixIcon: Icon(
                                Icons.lock_outline,
                              ),
                              suffixIcon: new GestureDetector(
                                onTap: () {setState(() {_obscureText = !_obscureText;});},
                                child: new Icon(_obscureText ? Icons.visibility : Icons.visibility_off),
                              )
                          ),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            TextButton(
                              onPressed: () {},
                              child: Text('Forget Password? ',
                                  style: Theme.of(context)
                                      .textTheme
                                      .subtitle1!
                                      .copyWith(
                                          fontSize: 15,
                                          fontWeight: FontWeight.bold)),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 30.0,
                        ),

                        isloading==false?   ConditionalBuilder(
                          condition: State is! LoginStates,
                          builder: (context) => Center(
                            child: Container(
                              width: double.infinity,
                              height: 50.0,
                              child:
                              ElevatedButton(
                                style: ElevatedButton.styleFrom(
                                  primary: Colors.indigo,
                                  shape: new RoundedRectangleBorder(
                                    borderRadius:
                                        new BorderRadius.circular(30.0),
                                  ),
                                ),
                                child: Text(
                                  'Login',
                                  style: TextStyle(fontSize: 30.0),
                                ),
                                onPressed: () {
                                  if (formKey.currentState!.validate()) {
                                    Login();
                                  }
                                },
                              ),
                            ),
                          ),
                        ) :CircularProgressIndicator(),
                        SizedBox(height: 30.0),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              'Don\'t have an account ? ',
                            ),
                            TextButton(
                              onPressed: () {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => SginupScreen()),
                                );
                              },
                              child: Text('Register Now '),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          );
        },
      ),
    );
  }

  void Login() async {

      setState(() {isloading = true;});
      DioUtil.createDio().post("UsersRigester/Login",data: {"Email": emailController.text,"Password":passwordController.text}).then((response) async {
        if(response.statusCode == 200)
        {
          setState(() {isloading = false;});
          var body=User.fromJson(response.data);
          if(body.id!=0)
          {
            if(body.rule==2)
              {
                // SharedPreferences preferences = await SharedPreferences.getInstance();
                // preferences.setInt("id",body!.id!);
                Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (BuildContext ctx)=> homelayout(body)), (route) => false);
              }
            else if(body.rule==1)
              {
                Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (BuildContext ctx)=>VoultneerHomeScreen()), (route) => false);
              }

          }
          else  if(body.id==0)
          {
            scaffoldKey.currentState!.showSnackBar(
              SnackBar(
                content: Text("wrong user name or password"),
              ),
            );
          }
        }
        else {
          setState(() {isloading = false;});
          scaffoldKey.currentState!.showSnackBar(
            SnackBar(
              content: Text("wrong user name or password"),
            ),
          );
        }
      });
    }
}
