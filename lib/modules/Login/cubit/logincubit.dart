import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:gp/dio/DioHelper.dart';
import 'package:gp/dio/endpoint.dart';
import 'package:gp/models/loginmodel.dart';
import 'package:gp/modules/Login/cubit/loginstatuts.dart';


class LoginCubit extends Cubit<LoginStates> {
  LoginCubit() : super(LoginInitialState());

  static LoginCubit get(context) => BlocProvider.of(context);

  late LoginModel loginModel;

  void userLogin({
    // ignore: non_constant_identifier_names
    required String email,
    required String password,
  }) {
    emit(LoginLoadingState());

    DioHelper.postData(
      url: LOGIN,
      data: {
        'email': email,
        'password': password,
      },
    ).then((value) {
      // ignore: avoid_print
      print(value.data);
      // ignore: avoid_print

      loginModel = LoginModel.fromJson(value.data);
      //ignore: avoid_print
      print('Message is ${loginModel} ');
      emit(LoginSuccessState(loginModel));
    }).catchError((error) {
      // ignore: avoid_print
      print(error.toString());
      emit(LoginErrorState(error.toString()));
    });
  }

  bool isPasswordShown = true;
  IconData passwordIcon = Icons.visibility_outlined;

  void showPassword() {
    isPasswordShown = !isPasswordShown;

    passwordIcon = isPasswordShown
        ? Icons.visibility_outlined
        : Icons.visibility_off_outlined;

   emit(PasswordShownSuccess());
  }
}