import 'dart:io';

import 'package:flutter/material.dart';
import 'package:gp/Network/DioUtil.dart';
import 'package:gp/models/User.dart';
import 'package:image_picker/image_picker.dart';
import 'package:permission_handler/permission_handler.dart';

class  Profile extends StatefulWidget {
  int id;
   Profile(this.id) ;

  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  final formKey = GlobalKey<FormState>();
  TextEditingController  nameController = TextEditingController();
  TextEditingController  emailController = TextEditingController();
  TextEditingController  phoneController = TextEditingController();
  TextEditingController  passwordController = TextEditingController();
  TextEditingController  ageController = TextEditingController();

  bool _address = true;
  final FocusNode myFocusNode = FocusNode();

  User? _user;
  bool isloading=false;

  @override
  void initState() {
    super.initState();
    getprofiledata();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
        appBar: AppBar(
      backgroundColor: Colors.indigo ,
      title: Text(
        '  Disabeled profile  ',
        style: TextStyle(
          fontSize: (25.0),
          fontWeight: FontWeight.bold,

        ),
      ),
    ),
      body: Padding(
        padding: EdgeInsets.only(top: 30.0,right: 20,left: 20 ,bottom: 20),
        child: Center(
          child: SingleChildScrollView(
            child: Form(
              key: formKey, //دا اللى هيشغلى اللى  validator
              child: Column(
                children: <Widget>[
                  Column(
                    children: <Widget>[
                      new Container(
                        height: 250.0,
                        color: Colors.white,
                        child: new Column(
                          children: <Widget>[
                            Padding(
                                padding: EdgeInsets.only(left: 15.0, top: 5.0),
                                child: new Row(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    new Icon(
                                      Icons.arrow_back_ios,
                                      color: Colors.black,
                                      size: 22.0,
                                    ),
                                    Padding(
                                      padding: EdgeInsets.only(left: 25.0),
                                    )
                                  ],
                                )),
                            Padding(
                              padding: EdgeInsets.only(top: 20.0),
                              child: new Stack(fit: StackFit.loose, children: <Widget>[
                                new Row(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment. start,
                                  children: <Widget>[
                                    new Container(
                                        width: 140.0,
                                        height: 140.0,
                                        decoration: new BoxDecoration(
                                          shape: BoxShape.circle,
                                          image: new DecorationImage(
                                            image: new ExactAssetImage(
                                                'assets/images/male.png'),
                                            fit: BoxFit.cover,
                                          ),
                                        )),
                                  ],
                                ),
                                Padding(
                                    padding: EdgeInsets.only(top: 90.0, right: 100.0),
                                    child: new Row(
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      children: <Widget>[
                                        new CircleAvatar(
                                          backgroundColor: Colors.indigo,
                                          radius: 27.0,
                                          child: new Icon(
                                            Icons.camera_alt,
                                            color: Colors.white,
                                          ),
                                        )
                                      ],
                                    )),
                              ]),
                            )
                          ],
                        ),
                      ),
                      new Container(
                        color: Color(0xffFFFFFF),
                        child: Padding(
                          padding: EdgeInsets.only(bottom: 25.0),
                          child: new Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              Padding(
                                  padding: EdgeInsets.only(
                                      left: 15.0, right: 20.0, top: 25.0),
                                  child: new Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    mainAxisSize: MainAxisSize.max,
                                    children: <Widget>[
                                      new Column(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        mainAxisSize: MainAxisSize.min,
                                        children: <Widget>[
                                          new Text(
                                            'Parsonal Information',
                                            style: TextStyle(
                                                fontSize: 25.0,
                                                fontWeight: FontWeight.bold),
                                          ),
                                        ],
                                      ),
                                      new Column(
                                        mainAxisAlignment: MainAxisAlignment.end,
                                        mainAxisSize: MainAxisSize.min,
                                        children: <Widget>[
                                          _address ? _getEditIcon() : new Container(),
                                        ],
                                      )
                                    ],
                                  )),
                              Padding(
                                  padding: EdgeInsets.only(
                                      left: 25.0, right: 25.0, top: 25.0),
                                  child: new Row(
                                    mainAxisSize: MainAxisSize.max,
                                    children: <Widget>[
                                      new Column(
                                        mainAxisAlignment: MainAxisAlignment.start,
                                        mainAxisSize: MainAxisSize.min,
                                        children: <Widget>[
                                          new Text(
                                            'Name',
                                            style: TextStyle(
                                                fontSize: 20.0,
                                                fontWeight: FontWeight.bold),
                                          ),
                                        ],
                                      ),
                                    ],
                                  )),
                              Padding(
                                  padding: EdgeInsets.only(
                                      left: 25.0, right: 25.0, top: 2.0),
                                  child: new Row(
                                    mainAxisSize: MainAxisSize.max,
                                    children: <Widget>[
                                      new Flexible(
                                        child: new TextFormField(
                                          decoration: const InputDecoration(
                                            hintText: "Enter Your Name",
                                          ),
                                          enabled: !_address,
                                          autofocus: !_address,
                                          controller: nameController,

                                            validator: (value) {
                                              if (value!.trim().length > 25) {
                                                return 'Username must be at least 25characters in length';
                                              }
                                              if (value.trim().isEmpty) {
                                                return 'This field is required';
                                              }
                                              return null;
                                            }
                                        ),
                                      ),
                                    ],
                                  )),

                              Padding(
                                  padding: EdgeInsets.only(
                                      left: 25.0, right: 25.0, top: 25.0),
                                  child: new Row(
                                    mainAxisSize: MainAxisSize.max,
                                    children: <Widget>[
                                      new Column(
                                        mainAxisAlignment: MainAxisAlignment.start,
                                        mainAxisSize: MainAxisSize.min,
                                        children: <Widget>[
                                          new Text(
                                            'Email ',
                                            style: TextStyle(
                                                fontSize: 16.0,
                                                fontWeight: FontWeight.bold),
                                          ),
                                        ],
                                      ),
                                    ],
                                  )),
                              Padding(
                                  padding: EdgeInsets.only(
                                      left: 25.0, right: 25.0, top: 2.0),
                                  child: new Row(
                                    mainAxisSize: MainAxisSize.max,
                                    children: <Widget>[
                                      new Flexible(
                                        child: new TextFormField(
                                          decoration: const InputDecoration(
                                              hintText: "Enter Email ID"),
                                          enabled: !_address,
                                          controller: emailController,
                                            validator: (value) {
                                              if (value == null) return 'enter your email';
                                              if (value != null) {
                                                if (value.length > 5 && value.contains('@') && value.endsWith('.com')) {
                                                  return null;
                                                }
                                                return 'Enter a Valid Email Address';
                                              }
                                            }
                                        ),
                                      ),
                                    ],
                                  )),
                              Padding(
                                  padding: EdgeInsets.only(
                                      left: 25.0, right: 25.0, top: 25.0),
                                  child: new Row(
                                    mainAxisSize: MainAxisSize.max,
                                    children: <Widget>[
                                      new Column(
                                        mainAxisAlignment: MainAxisAlignment.start,
                                        mainAxisSize: MainAxisSize.min,
                                        children: <Widget>[
                                          new Text(
                                            'Phone',
                                            style: TextStyle(
                                                fontSize: 20.0,
                                                fontWeight: FontWeight.bold),
                                          ),
                                        ],
                                      ),
                                    ],
                                  )),
                              Padding(
                                  padding: EdgeInsets.only(
                                      left: 25.0, right: 25.0, top: 2.0),
                                  child: new Row(
                                    mainAxisSize: MainAxisSize.max,
                                    children: <Widget>[
                                      new Flexible(
                                        child: new TextFormField(
                                          decoration: const InputDecoration(
                                              hintText: "Enter phone Number"),
                                          enabled: !_address,
                                          controller: phoneController,
                                          validator: (value) {
                                            if(value==null) return "enter your phone";
                                            if (value!.length <10 ) {
                                              return 'phone number is short';
                                            }
                                            return null;
                                          },
                                        ),
                                      ),
                                    ],
                                  )),
                              // Padding(
                              //     padding: EdgeInsets.only(
                              //         left: 25.0, right: 25.0, top: 25.0),
                              //     child: new Row(
                              //       mainAxisSize: MainAxisSize.max,
                              //       children: <Widget>[
                              //         new Column(
                              //           mainAxisAlignment: MainAxisAlignment.start,
                              //           mainAxisSize: MainAxisSize.min,
                              //           children: <Widget>[
                              //             new Text(
                              //               'kind of disability  ',
                              //               style: TextStyle(
                              //                   fontSize: 20.0,
                              //                   fontWeight: FontWeight.bold),
                              //             ),
                              //           ],
                              //         ),
                              //       ],
                              //     )),
                              // Padding(
                              //     padding: EdgeInsets.only(
                              //         left: 25.0, right: 25.0, top: 2.0),
                              //     child: new Row(
                              //       mainAxisSize: MainAxisSize.max,
                              //       children: <Widget>[
                              //         new Flexible(
                              //           child: new TextField(
                              //             decoration: const InputDecoration(
                              //               hintText: "Enter Your kind of disability ",
                              //             ),
                              //             enabled: !_address,
                              //             autofocus: !_address,
                              //
                              //           ),
                              //         ),
                              //       ],
                              //     )),

                              // Padding(
                              //     padding: EdgeInsets.only(
                              //         left: 25.0, right: 25.0, top: 25.0),
                              //     child: new Row(
                              //       mainAxisSize: MainAxisSize.max,
                              //       mainAxisAlignment: MainAxisAlignment.start,
                              //       children: <Widget>[
                              //         Expanded(
                              //           child: Container(
                              //             child: new Text(
                              //               ' gender ',
                              //               style: TextStyle(
                              //                   fontSize: 20.0,
                              //                   fontWeight: FontWeight.bold),
                              //             ),
                              //           ),
                              //           flex: 2,
                              //         ),
                              //         Expanded(
                              //           child: Container(
                              //             child: new Text(
                              //               'address',
                              //               style: TextStyle(
                              //                   fontSize: 20.0,
                              //                   fontWeight: FontWeight.bold),
                              //             ),
                              //           ),
                              //           flex: 2,
                              //         ),
                              //       ],
                              //     )),
                              // Padding(
                              //     padding: EdgeInsets.only(
                              //         left: 25.0, right: 25.0, top: 2.0),
                              //     child: new Row(
                              //       mainAxisSize: MainAxisSize.max,
                              //       mainAxisAlignment: MainAxisAlignment.start,
                              //       children: <Widget>[
                              //         Flexible(
                              //           child: Padding(
                              //             padding: EdgeInsets.only(right: 10.0),
                              //             child: new TextField(
                              //               decoration: const InputDecoration(
                              //                   labelText: "enter your address "),
                              //               enabled: !_address,
                              //             ),
                              //           ),
                              //           flex: 2,
                              //         ),
                              //         Flexible(
                              //           child: new TextField(
                              //             decoration: const InputDecoration(
                              //                 hintText: "Enter State"),
                              //             enabled: !_address,
                              //           ),
                              //           flex: 2,
                              //         ),
                              //       ],
                              //     )),
                              !_address ? _getActionButtons() : new Container(),
                            ],
                          ),
                        ),
                      )
                    ],
                  ),
                ],
              )
            ),
          ),
        ),
      ),

    );
  }

  @override
  void dispose() {
    // Clean up the controller when the Widget is disposed
    myFocusNode.dispose();
    super.dispose();
  }

  Widget _getActionButtons() {
    return Padding(
      padding: EdgeInsets.only(left: 25.0, right: 25.0, top: 45.0),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Expanded(
            child: Padding(
              padding: EdgeInsets.only(right: 10.0),
              child: Container(
                  child:isloading==false? new RaisedButton(
                    child: new Text("Save"),
                    textColor: Colors.white,
                    color: Colors.indigo,
                    onPressed: () {
                      setState(() {
                      if(formKey.currentState!.validate()) {
                        updateprofiledata();
                        _address = true;
                        FocusScope.of(context).requestFocus(new FocusNode());
                      }


                      });
                    },
                    shape: new RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(20.0)),
                  ):Center(
                    child: CircularProgressIndicator(),
                  )),
            ),
            flex: 2,
          ),
          Expanded(
            child: Padding(
              padding: EdgeInsets.only(left: 10.0),
              child: Container(
                  child: new RaisedButton(
                    child: new Text("Cancel"),
                    textColor: Colors.white,
                    color: Colors.indigo,
                    onPressed: () {
                      setState(() {
                        _address = true;
                        FocusScope.of(context).requestFocus(new FocusNode());
                      });
                    },
                    shape: new RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(20.0)),
                  )),
            ),
            flex: 2,
          ),
        ],
      ),
    );
  }

  Widget _getEditIcon() {
    return new GestureDetector(
      child: new CircleAvatar(
        backgroundColor: Colors.indigo,
        radius: 20.0,
        child: new Icon(
          Icons.edit,
          color: Colors.white,
          size: 16.0,
        ),
      ),
      onTap: () {
        setState(() {
          _address = false;
        });
      },
    );
  }
  void getprofiledata() async
  {
    DioUtil.createDio().get("UsersRigester/GetById/${widget.id}").then((response)
    {
      _user=User.fromJson(response.data);
      if(response.statusCode==200)
        {
          nameController.text=_user!.name!;
          emailController.text=_user!.email!;
          ageController.text=_user!.age!.toString();
          phoneController.text=_user!.phone!;
      print(response.data);
      setState(() {});
      }
    });
  }
  void updateprofiledata() async
  {
    setState(() {isloading = true;});
    DioUtil.createDio().put("UsersRigester/Updete",data: {
      "Id": widget.id,
      "Name": "${nameController.text}",
      "Phone": "${phoneController.text}",
      "Email": "${emailController.text}",
      "Gender":"${_user!.gender==null?"": _user!.gender}" ,
      "Password": "${_user!.password}",
      "PhotoUrl": "image.png",
      "Age": _user!.age,
      "Rule": _user!.rule,
      "Pointes": 0
    }).then((response)
    {
      if(response.statusCode==200)
      {
        setState(() {isloading = false;});
        if(response.data==true)
          {
            getprofiledata();
            okAlter(context,"update","profile updated");
          }

      }
    }
    );

  }

  Future<void> okAlter(BuildContext context,String title,String contain)
  {
    return showDialog<void>(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(title),
          content: Text(contain),
          actions: <Widget>[
            ElevatedButton(
              child: Text('OK'),
              onPressed: ( ) {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
}

