import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_image_slideshow/flutter_image_slideshow.dart';
import 'package:gp/styels/constants.dart';
import 'package:gp/styels/design.dart';


class HomeScreenDisable extends StatefulWidget {
  const HomeScreenDisable({Key? key}) : super(key: key);

  @override
  _HomeScreenDisableState createState() => _HomeScreenDisableState();
}
class _HomeScreenDisableState extends State<HomeScreenDisable> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
appBar: AppBar(
  backgroundColor: Colors.white,
  leading: Icon(
    Icons.menu,
    color: Colors.black,
    size: 40,
  ),
),
body:
  SingleChildScrollView(
    child: Column(
    children: [
    SafeArea(
    child: ImageSlideshow(
      height: 230,
      initialPage: 1,
      indicatorColor: Colors.indigo,
      indicatorBackgroundColor: Colors.grey,
      children: [
        Image.asset(
          'assets/images/imoo.jpg',
          fit: BoxFit.fill,
        ),
        Image.asset(
          'assets/images/imoo.jpg',
          fit: BoxFit.fill,
        ),
        Image.asset(
          'assets/images/imoo.jpg',
          fit: BoxFit.fill,
        ),
      ],
      onPageChanged: (value) {
        //print('Page changed: $value');
      },
      autoPlayInterval: 3000,
      isLoop: true,
    ),

    ),
      SizedBox( height: 20,),
      Container(
        child:Column (
          crossAxisAlignment: CrossAxisAlignment.start,
       children: [
         DesignText(text: ' Services ',size: 40,color: mainColor,),
        SizedBox( height: 20,),

         Padding(
           padding: const EdgeInsets.only(left: 8.0, right: 8.0),
           child: Row(
             mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Column(
                children: [
                  Icon(
                    Icons.ac_unit,
                    size: 80,

                  ),
                  DesignText(text: ' Services ', size: 20,color:mainColor ,)
                ],
              ),
              Column(
                children: [
                  Icon(
                    Icons.ac_unit,
                    size: 80,

                  ),
                  DesignText(text: ' Services ', size: 20,) ,

                ],
              ),
              Column(
                children: [
                  Icon(
                    Icons.ac_unit,
                    size: 80,

                  ),
                  DesignText(text: ' Services ', size: 20,color:mainColor ,) ,

                ],
              ),
            ],
        ),
         ),
        SizedBox(
          height: 25,
        ),
         Padding(
           padding: const EdgeInsets.only(left: 8.0, right: 8.0),
           child: Row(
             mainAxisAlignment: MainAxisAlignment.spaceBetween,
             children: [
               Column(
                 children: [
                   Icon(
                     Icons.ac_unit,
                     size: 80,

                   ),
                   DesignText(text: ' Services ', size: 20,) ,

                 ],
               ),
               Column(
                 children: [
                   Icon(
                     Icons.ac_unit,
                     size: 80,

                   ),
                   DesignText(text: ' Services ', size: 20,) ,

                 ],
               ),
               Column(
                 children: [
                   Icon(
                     Icons.ac_unit,
                     size: 80,

                   ),
                   DesignText(text: ' Services ', size: 20,) ,

                 ],
               ),
             ],
           ),
         ),
       ],
        ),
      ),

    ],



    ),
),





        );

  }
}
