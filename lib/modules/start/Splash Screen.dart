import 'dart:async';
import 'package:flutter/material.dart';
import 'package:gp/modules/Login/Login%20Screen.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  _SplashScreenState createState() => _SplashScreenState();
}
class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
    Timer(
      Duration(seconds: 5),
      () => Navigator.of (context).pushReplacement(
        MaterialPageRoute(
          builder: (BuildContext context) => LoginScreen(),
        ),
      ),
    );
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: null,
      body: Container(
        color: Colors.indigo,
        child: Center(
          child: CircleAvatar(
            radius: 150,
            backgroundImage:
            AssetImage(
              'assets/images/applogo.jpeg'
            ),
          ),
     ),
      ),
    );
  }
}
