import 'package:flutter/material.dart';
import 'package:gp/Dio/DioHelper.dart';
import 'package:gp/modules/DisableHomeScreen/ProfileDisabledScreen.dart';
import 'package:gp/modules/Login/Login%20Screen.dart';
import 'package:gp/modules/start/Splash%20Screen.dart';



void main() {
  runApp(const MyApp());
  DioHelper.init();
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: LoginScreen(),
    );
  }
}
