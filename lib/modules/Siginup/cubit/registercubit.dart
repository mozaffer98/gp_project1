
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:gp/dio/DioHelper.dart';
import 'package:gp/dio/endpoint.dart';
import 'package:gp/models/registermodel.dart';
import 'package:gp/modules/Siginup/cubit/registerstatus.dart';

class RegisterCubit extends Cubit<RegisterStates> {
  RegisterCubit() : super(RegisterInitialState());

  static RegisterCubit get(context) => BlocProvider.of(context);

  late RegisterModel registerModel;

  void userRegister({
    required String Name ,
    required String Email ,
    required String Password,
    required String  Phone,
    required String  Gender,
    required String  PhotoUrl,
    required String Age ,
    required String Rule ,






  }) {
    emit(RegisterLoadingState());

    DioHelper .postData(
      url: REGISTER,
      data: {
        'name':Name ,
        'email': Email,
        'password': Password,
        'Phone': Phone,
        'Gender': Gender,
        'PhotoUrl': PhotoUrl,
        'Age': Age,
        'Rule': Rule,



      },
    ).then((value) {
      // ignore: avoid_print
      print(value.data);
      registerModel = RegisterModel.fromJson(value.data);
      emit(RegisterSuccessState(registerModel));
    }).catchError((error) {
      // ignore: avoid_print
      print(error.toString());
      emit(RegisterErrorState(error.toString()));
    });
  }

  bool isPasswordShown = true;
  IconData passwordIcon = Icons.visibility_outlined;


  void showPassword() {
    isPasswordShown = !isPasswordShown;

    passwordIcon = isPasswordShown
        ? Icons.visibility_outlined
        : Icons.visibility_off_outlined;

    emit(PasswordShownSuccess());
  }
}