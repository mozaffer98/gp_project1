import 'package:flutter/material.dart';
import 'package:gp/Network/DioUtil.dart';
import 'package:gp/models/User.dart';
import 'package:gp/modules/DisableHomeScreen/homeScreendisable.dart';
import 'package:gp/modules/VoultneerHome/voultneerhome.dart';
import 'package:gp/shared/components/mybutton.dart';

class SginupScreen extends StatefulWidget {
  @override
  _SginupScreenState createState() => _SginupScreenState();
}

class _SginupScreenState extends State<SginupScreen> {
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  final formKey = GlobalKey<FormState>();
  List<String> gender = ["Male", "Female"];
  var _gender;
  Map<String, int> type = {"volunteer": 1, "disabled": 2};
  var _type;
  TextEditingController nameController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController phoneController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController ConfirmPasswordController = TextEditingController();
  TextEditingController gendercontroller = TextEditingController();
  TextEditingController agecontroller = TextEditingController();

  String Password = "";
  bool _obscureText = true;
  bool _obscureText1 = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
      appBar: AppBar(
        title: Text("Register"),
        centerTitle: true,
        backgroundColor: Colors.blue,
      ),
      body: Padding(
        padding: EdgeInsets.only(top: 30.0,right: 20,left: 20 ,bottom: 20),
        child: Center(
          child: SingleChildScrollView(
            child: Form(
              key: formKey, //دا اللى هيشغلى اللى  validator
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
          // user name
                  TextFormField(
                    controller: nameController,
                    validator: (value) {
                      if (value!.trim().length > 25) {
                        return 'Username must be at least 25characters in length';
                      }
                      if (value.trim().isEmpty) {
                        return 'This field is required';
                      }
                      return null;
                    },
                    keyboardType: TextInputType.text,
                    decoration: InputDecoration(
                      labelText: 'User name',
                      border: OutlineInputBorder(),
                      prefixIcon: Icon(
                        Icons.person,
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 12.0,
                  ),
          //Email
                  TextFormField(
                    controller: emailController,
                    validator: (value) {
                      if (value == null) return 'enter your email';
                        if (value != null) {
                        if (value.length > 5 && value.contains('@') && value.endsWith('.com')) {
                          return null;
                        }
                        return 'Enter a Valid Email Address';
                      }
                    },
                    //  onChanged: (),
                    keyboardType: TextInputType.emailAddress,
                    decoration: InputDecoration(
                      labelText: 'email',
                      prefixIcon: Icon(
                        Icons.email_outlined,
                      ),
                      border: OutlineInputBorder(),
                    ),
                  ),
                  const SizedBox(
                    height: 12.0,
                  ),
//Password
                  TextFormField(
                    controller: passwordController,
                    validator: (value) {
                      if (value!.length > 8 || value.isEmpty) {
                        return ' enter your valid password ';
                      }
                      setState(() {Password=value;});
                      return null;
                    },
                    obscureText: _obscureText1,
                    keyboardType: TextInputType.visiblePassword,
                    decoration: InputDecoration(
                        labelText: 'Password',
                        border: OutlineInputBorder(),
                        prefixIcon: Icon(
                          Icons.lock_outline,
                        ),
                        suffixIcon: new GestureDetector(
                          onTap: () {
                            setState(() {
                              _obscureText1 = !_obscureText1;
                            });
                          },
                          child: new Icon(_obscureText1
                              ? Icons.visibility
                              : Icons.visibility_off),
                        )),

                    //   onChanged: ()
                  ),
                  const SizedBox(
                    height: 12.0,
                  ),
                  //confirm
                  TextFormField(
                    controller: ConfirmPasswordController,
                    validator: (value) {
                      if (value != Password) {
                        return '  Confirmation  password  dose not match the entered password';
                      }
                      return null;
                    },
                    keyboardType: TextInputType.visiblePassword,
                    obscureText: _obscureText,
                    // عشان الباسورد ميظهرش
                    decoration: InputDecoration(
                        labelText: ' ConfirmPassword',
                        border: OutlineInputBorder(),
                        prefixIcon: Icon(
                          Icons.lock_outline,
                        ),
                        suffixIcon: new GestureDetector(
                          onTap: () {
                            setState(() {
                              _obscureText = !_obscureText;
                            });
                          },
                          child: new Icon(_obscureText
                              ? Icons.visibility
                              : Icons.visibility_off),
                        )),
                  ),
                  const SizedBox(
                    height: 12.0,
                  ),
//Phone
                  TextFormField(
                    controller: phoneController,
                    keyboardType: TextInputType.number,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Phone ',
                      prefixIcon: Icon(Icons.phone_android_outlined),
                    ),
                    validator: (value) {
                      if(value==null) return "enter your phone";
                      if (value!.length <10 ) {
                        return 'phone number is short';
                      }
                      return null;
                    },
                  ),
                  const SizedBox(
                    height: 12.0,
                  ),
                  TextFormField(
                    controller: agecontroller,
                    keyboardType: TextInputType.number,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Age ',
                      prefixIcon: Icon(Icons.date_range_outlined),
                    ),
                    validator: (value) {
                      if(value==null) return "enter your age";
                      if (value!.length >3 ) {
                        return 'age is long';
                      }
                      return null;
                    },
                  ),
                  Container(
                    padding: EdgeInsets.only(top: 20),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Expanded(
                          child: Container(
                            height: 58,
                            child: InputDecorator(
                              decoration: const InputDecoration(
                                  border: OutlineInputBorder()),
                              child: DropdownButtonHideUnderline(
                                child: ButtonTheme(
                                    alignedDropdown: true,
                                    child: DropdownButton(
                                      hint: Text(
                                          'Please choose a gender'), // Not necessary for Option 1
                                      value: _gender,
                                      onChanged: (newValue) {
                                        setState(() {
                                          _gender = newValue;
                                          print(_gender);
                                        });
                                      },
                                      items: gender.map((location) {
                                        return DropdownMenuItem(
                                          child: new Text(location),
                                          value: location,
                                        );
                                      }).toList(),
                                    )),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(top: 20),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Expanded(
                          child: Container(
                            height: 58,
                            child: InputDecorator(
                              decoration: const InputDecoration(
                                  border: OutlineInputBorder()),
                              child: DropdownButtonHideUnderline(
                                child: ButtonTheme(
                                  alignedDropdown: true,
                                  child: DropdownButton<int>(
                                    hint: Center(child: Text('select type')),
                                    items: type
                                        .map((description, value) {
                                          return MapEntry(
                                              description,
                                              DropdownMenuItem<int>(
                                                value: value,
                                                child: Text(description),
                                              ));
                                        })
                                        .values
                                        .toList(),
                                    value: _type,
                                    onChanged: (int? newValue) {
                                      if (newValue != null) {
                                        setState(() {
                                          _type = newValue;
                                          print(_type);
                                        });
                                      }
                                    },
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  const SizedBox(
                    height: 12.0,
                  ),
                  isloading == false
                      ? Container(
                          margin: EdgeInsets.only(top: 20),
                          height: 45,
                          width: double.infinity,
                          child: MyButton(
                            title: "Sign Up",
                            function: () {
                              if (formKey.currentState!.validate() &&
                                  _gender != null &&
                                  _type != null) {
                                submit(
                                    FullName: nameController.text,
                                    Phone: phoneController.text,
                                    Email: emailController.text,
                                    Password: ConfirmPasswordController.text,
                                    Gender: _gender,
                                    Age: int.parse(agecontroller.text),
                                    Rule: _type);
                              }
                            },
                            heigth: 200,
                          ),
                        )
                      : Center(
                          child: CircularProgressIndicator(),
                        ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  bool isloading = false;

  void submit(
      {required String FullName,
      required String Phone,
      required String Email,
      required String Gender,
      required String Password,
      required int Age,
      required int Rule}) async {
    setState(() {
      isloading = true;
    });
    DioUtil.createDio().post("UsersRigester/Insert", data: {
      "Name": "$FullName",
      "Phone": "$Phone",
      "Email": "$Email",
      "Gender": "$Gender",
      "Password": "$Password",
      "PhotoUrl": "image.png",
      "Age": Age,
      "Rule": Rule
    }).then((response) {
      if (response.statusCode == 200) {
        setState(() {
          isloading = false;
        });
        //
        var body = User.fromJson(response.data);
        if (body.id != 0) {
          if (body.rule == 2) {
            Navigator.pushAndRemoveUntil(
                context,
                MaterialPageRoute(
                    builder: (BuildContext ctx) => HomeScreenDisable()),
                (route) => false);
          } else if (body.rule == 1) {
            Navigator.pushAndRemoveUntil(
                context,
                MaterialPageRoute(
                    builder: (BuildContext ctx) => VoultneerHomeScreen()),
                (route) => false);
          }
        } else {
          setState(() {
            isloading = false;
          });
          var scaffoldKey;
          scaffoldKey.currentState!.showSnackBar(SnackBar(
            content: Text("Connection Error"),
          ));
        }
      }
    });
  }

  Future<void> okAlter(BuildContext context, String title, String contain) {
    return showDialog<void>(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(title),
          content: Text(contain),
          actions: <Widget>[
            ElevatedButton(
              child: Text('OK'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
}
