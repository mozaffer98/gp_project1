import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:flutter/material.dart';
import 'package:gp/Network/DioUtil.dart';
import 'package:gp/homelayout/Screen%20request/DateTime.dart';
import 'package:gp/models/User.dart';
import 'package:intl/intl.dart';


class DisableRequest extends StatefulWidget {
  User? user;
  DisableRequest(this.user) ;
  @override
  _DisableRequestState createState() => _DisableRequestState();
}

class _DisableRequestState extends State<DisableRequest> {
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  final formKey = GlobalKey<FormState>();

  TextEditingController phoneController = TextEditingController();
  TextEditingController descriptionController = TextEditingController();
  TextEditingController priceController = TextEditingController();



  bool isloading=false;


  int? point;
  final Pay = {'Free', 'Paid'};
  String? pay;
  final typeofservices = {'Home Service', 'Translation Service','Delivery Service','Rehabilitation Servise','Orher'};
  String? _typeofservices,date,time;

  DropdownMenuItem<String> paymenuitem(String item) => DropdownMenuItem(
    value: item,
    child: Text(
      item,
      style: TextStyle(
        fontSize: 20,
      ),
    ),
  );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
      appBar: AppBar(
        backgroundColor: Colors.white,
        leading:  IconButton(
          icon: const Icon(Icons.arrow_back, color: Colors.indigo),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
        title: const Text(
          ' Request page  ',
          style: TextStyle(
            fontSize: 35.0,
            color: Colors.indigo,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
      body: Padding(
          padding: const EdgeInsets.all(16.0),
          child: SingleChildScrollView(
            child: Form(
              key:formKey ,
              child: Column(
                  children: [
                    Container(
                        child: BasicDateField(),
                    ),
                    const SizedBox(height: 15,),
                     Container(child: BasicTimeField()),
                    const SizedBox(height: 15,),
                    DropdownButtonFormField<String>(
                      hint: Text('Select type of services'),
                      decoration: InputDecoration(
                          enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.grey, width: 2),
                            borderRadius: BorderRadius.circular(10),
                          )),
                      value: _typeofservices,
                      items: typeofservices.map(paymenuitem).toList(),
                      onChanged: (value) => setState(() => this._typeofservices = value),
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return 'Please choose servise';
                        }
                        return null;
                      },

                    ),
                    const SizedBox(height: 15,),
                    DropdownButtonFormField<String>(
                      hint: Text('Select Payment'),
                      decoration: InputDecoration(
                          enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.grey, width: 2),
                            borderRadius: BorderRadius.circular(10),
                          )),
                      value: pay,
                      items: Pay.map(paymenuitem).toList(),
                      onChanged: (value) => setState(() => this.pay = value),
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return 'Please choose payment';
                        }
                        return null;
                      },

                    ),
                    const SizedBox(height: 15,),
                    pay=="Paid"?
                    Container(
                      margin: EdgeInsets.only(bottom: 15),
                      child: TextFormField(
                        keyboardType: TextInputType.number,
                        decoration: const InputDecoration(
                          labelText: ' Enter price ',
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.all(Radius.circular(7)),
                          ),
                          prefixIcon: Icon(
                            Icons.monetization_on_outlined,
                          ),
                        ),
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'Please enter your price';
                          }
                          return null;
                        },
                      ),
                    ):Container(),
                    TextFormField(
                      decoration: const InputDecoration(
                        labelText: ' Enter your Location  ',
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(7)),
                        ),
                        prefixIcon: Icon(
                          Icons.fmd_good_outlined,
                        ),
                      ),
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return 'Please enter your address';
                        }
                        return null;
                      },
                    ),
                    const SizedBox(height: 30,),
                    TextFormField(
                      controller: phoneController,
                      decoration: const InputDecoration(
                        labelText: ' your phone  ',
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(7)),

                        ),
                        prefixIcon: Icon(
                          Icons.phone,
                        ),
                      ),
                      keyboardType: TextInputType.number,

                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return 'Please enter your phone';
                        }
                        return null;
                      },
                    ),
                    const SizedBox(height: 30,),
                    TextFormField(
                      minLines: 2,
                      maxLines: 5,
                      keyboardType: TextInputType.multiline,
                      decoration: const InputDecoration(
                          labelText: ' enter your description  ',
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.all(Radius.circular(7)),
                          )),
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return 'Please enter description';
                        }
                        return null;
                      },
                    ),
                    const SizedBox(height: 50,),
                  isloading==false?  Center(
                      child: ElevatedButton(
                        onPressed: () {
                          if(formKey.currentState!.validate())
                            {
                                Ask_help();
                            }


                        },
                        style: ElevatedButton.styleFrom(
                          primary: Colors.indigo,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(30.0),
                          ),
                        ),
                        child: const Text(
                          ' Send Request ',
                          style: TextStyle(fontSize: 40.0),
                        ),
                      ),
                    ):CircularProgressIndicator(),
                  ]),
            ),
          )),
    );
  }//

  void Ask_help() async
  {
    setState(() {isloading = true;});
    DioUtil.createDio().post("AskeHelp/Insert",
        data:{
      "TypeOfServes": "${_typeofservices}",
      "Time": "${time}",
      "Date": "${date}",
      "Price": pay=="Pay"?priceController.text:0,
      "Pointes": point,
      "Describtion": "${descriptionController.text}",
      "IsDone": false,
      "UserId": widget.user!.id,
      "Longitude": 0,
      "Latitude": 0,
      "Zoom": 0,
      "Phone":"${ phoneController.text}",
      "Accepte": false,
      "VolunteerId": null
    } ).then((response)
    {
      if(response.statusCode==200)
        {
          setState(() {isloading = false;});
          okAlter(context,"done","Asked Help Done");
        }
      else
        {
          setState(() {isloading = false;});
          scaffoldKey.currentState!.showSnackBar(SnackBar(content: Text("Connection Error")));
        }
    }
    );
  }

  Future<void> okAlter(BuildContext context,String title,String contain)
  {
    return showDialog<void>(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(title),
          content: Text(contain),
          actions: <Widget>[
            ElevatedButton(
              child: Text('OK'),
              onPressed: ( ) {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }


}
