import 'package:flutter/material.dart';
class DesignText  extends StatelessWidget {
  double ? size ;
   String? text;
  Color ? color;


  DesignText ({Key? key, this.size,  required this.text, this.color,}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      text!,

      style: TextStyle(
        color : color,
        fontSize: size,
        fontWeight: FontWeight.bold,
      ),
    );
  }
}
