class RegisterModel {
  int? id;
  String? name;
  String? phone;
  String? email;
  String? gender;
  String? password;
  String? photoUrl;
  int? age;
  int? rule;

  RegisterModel(
      {this.id,
        this.name,
        this.phone,
        this.email,
        this.gender,
        this.password,
        this.photoUrl,
        this.age,
        this.rule});

  RegisterModel.fromJson(Map<String, dynamic> json) {
    id = json['Id'];
    name = json['Name'];
    phone = json['Phone'];
    email = json['Email'];
    gender = json['Gender'];
    password = json['Password'];
    photoUrl = json['PhotoUrl'];
    age = json['Age'];
    rule = json['Rule'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Id'] = this.id;
    data['Name'] = this.name;
    data['Phone'] = this.phone;
    data['Email'] = this.email;
    data['Gender'] = this.gender;
    data['Password'] = this.password;
    data['PhotoUrl'] = this.photoUrl;
    data['Age'] = this.age;
    data['Rule'] = this.rule;
    return data;
  }
}