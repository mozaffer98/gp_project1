class Help {
  int? id;
  String? typeOfServes;
  String? time;
  String? date;
  int? price;
  int? pointes;
  String? describtion;
  bool? isDone;
  int? userId;
  int? longitude;
  int? latitude;
  int? zoom;
  String? phone;
  bool? accepte;
  int? volunteerId;

  Help(
      {this.id,
        this.typeOfServes,
        this.time,
        this.date,
        this.price,
        this.pointes,
        this.describtion,
        this.isDone,
        this.userId,
        this.longitude,
        this.latitude,
        this.zoom,
        this.phone,
        this.accepte,
        this.volunteerId});

  Help.fromJson(Map<String, dynamic> json) {
    id = json['Id'];
    typeOfServes = json['TypeOfServes'];
    time = json['Time'];
    date = json['Date'];
    price = json['Price'];
    pointes = json['Pointes'];
    describtion = json['Describtion'];
    isDone = json['IsDone'];
    userId = json['UserId'];
    longitude = json['Longitude'];
    latitude = json['Latitude'];
    zoom = json['Zoom'];
    phone = json['Phone'];
    accepte = json['Accepte'];
    volunteerId = json['VolunteerId'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Id'] = this.id;
    data['TypeOfServes'] = this.typeOfServes;
    data['Time'] = this.time;
    data['Date'] = this.date;
    data['Price'] = this.price;
    data['Pointes'] = this.pointes;
    data['Describtion'] = this.describtion;
    data['IsDone'] = this.isDone;
    data['UserId'] = this.userId;
    data['Longitude'] = this.longitude;
    data['Latitude'] = this.latitude;
    data['Zoom'] = this.zoom;
    data['Phone'] = this.phone;
    data['Accepte'] = this.accepte;
    data['VolunteerId'] = this.volunteerId;
    return data;
  }
}
