class LoginModel {
  String? status;
  String? message;
  String? token;
  UserData? data;
  int? id;

  LoginModel.fromJson(Map<String, dynamic> json) {
    id = json['Id'];
    status = json['status'];
    message = json['message'];
    token = json['token'];
    data = json['data'] != null ? UserData.fromJson(json['data']) : null;
  }
}

class UserData {
  int? id;
  String? password;
  String? email;
  UserData.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    password = json['password'];
    email = json['email'];
  }
}