import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:flutter/material.dart';
import 'package:gp/models/User.dart';
import 'package:gp/modules/DisableHomeScreen/ActivityScreen.dart';
import 'package:gp/modules/DisableHomeScreen/ProfileDisabledScreen.dart';
import 'package:gp/modules/DisableHomeScreen/chatScreen.dart';
import 'package:gp/modules/DisableHomeScreen/homeScreendisable.dart';
import 'package:gp/modules/DisableRequest/DisableRequest.dart';
import 'package:gp/styels/constants.dart';


class homelayout extends StatefulWidget {

  User user;
  homelayout(this.user);

  @override
  _homelayoutState createState() => _homelayoutState();
}

class _homelayoutState extends State<homelayout> {
  int index  = 0;
  int currentindex=0;
  GlobalKey<CurvedNavigationBarState> _bottomNavigationKey = GlobalKey();
List? screens;

@override
  void initState() {
    // TODO: implement initState
    super.initState();
    screens=[
      HomeScreenDisable(),
      DisableRequest(widget.user),
      chatScreen(),
      ActivityScreen(),
      Profile(widget!.user!.id!),
    ];
  }
  @override
  Widget build(BuildContext context) {
    final items = <Widget>[
      const Icon(
        Icons.home,
        size: 30,
        color: secondaryColor,
      ),
      const Icon(
        Icons.add_box_sharp,
        size: 30,
        color: secondaryColor,
      ),
      const Icon(
        Icons.chat
        ,
        size: 30,
        color: secondaryColor,
      ),
      const Icon(
        Icons.local_activity,
        size: 30,
        color:secondaryColor ,
      ),
      const Icon(
        Icons.account_circle,
        size: 30,
        color: secondaryColor,
      ),
    ];


    return Scaffold(
      body: screens![currentindex],
        bottomNavigationBar: CurvedNavigationBar(
            key:_bottomNavigationKey ,
        color: const Color.fromARGB(255, 63, 81, 181),
         backgroundColor: Colors.transparent,
              items: items,
             height: 65,
              index: 0,
             onTap: (index) {
            setState(() {
              currentindex = index;
            }
            );
        }
        )
    );
  }
}
