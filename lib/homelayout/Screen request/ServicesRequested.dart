import 'package:flutter/material.dart';

class ServicesRequest extends StatefulWidget {
  const ServicesRequest({Key? key}) : super(key: key);
  @override
  _ServicesRequestState createState() => _ServicesRequestState();
}

class _ServicesRequestState extends State<ServicesRequest> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        leading: new IconButton(
          icon: new Icon(Icons.arrow_back, color: Colors.indigo),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
        title: Text(
          ' Requeste page  ',
          style: TextStyle(
            fontSize: 35.0,
            color: Colors.indigo,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
      body: Container(
          child: Padding(
              padding: const EdgeInsets.only(top: 40.0, left: 8, right: 8),
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    TextFormField(
                      decoration: const InputDecoration(
                        labelText: ' enter your Location  ',
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(7)),
                        ),
                        prefixIcon: Icon(
                          Icons.fmd_good_outlined,
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 40,
                    ),
                    TextFormField(
                      minLines: 2,
                      maxLines: 5,
                      keyboardType: TextInputType.multiline,
                      decoration: InputDecoration(
                          labelText: ' enter your description  ',
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.all(Radius.circular(7)),
                          )),
                    ),
                    SizedBox(
                      height: 330,
                    ),
                    Container(
                        child: Center(
                      child: Column(
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              ElevatedButton(
                                onPressed: () {},
                                style: ElevatedButton.styleFrom(
                                  primary: Colors.indigo,
                                  shape: new RoundedRectangleBorder(
                                    borderRadius:
                                        new BorderRadius.circular(30.0),
                                  ),
                                ),
                                child: Text(
                                  ' Save',
                                  style: TextStyle(fontSize: 30.0),
                                ),
                              ),
                              ElevatedButton(
                                onPressed: () {},
                                style: ElevatedButton.styleFrom(
                                  primary: Colors.indigo,
                                  shape: new RoundedRectangleBorder(
                                    borderRadius:
                                        new BorderRadius.circular(30.0),
                                  ),
                                ),
                                child: Text(
                                  ' Reset',
                                  style: TextStyle(fontSize: 30.0),
                                ),
                              )
                            ],
                          ),
                          SizedBox(
                            height: 30,
                          ),
                          Column(
                            children: [
                              ElevatedButton(
                                onPressed: () {},
                                style: ElevatedButton.styleFrom(
                                  primary: Colors.indigo,
                                  shape: new RoundedRectangleBorder(
                                    borderRadius:
                                        new BorderRadius.circular(30.0),
                                  ),
                                ),
                                child: Text(
                                  ' Send Request ',
                                  style: TextStyle(fontSize: 30.0),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ))
                  ]))),
    );
  }
}
