import 'package:dio/dio.dart';
class DioHelper {
  static Dio? dio;
  static init() async {
    dio = Dio(
      BaseOptions(
        baseUrl: "http://finalpromoahmed-001-site1.btempurl.com/",
        receiveDataWhenStatusError: true,
        validateStatus: (_) => true,
        headers: {
          "content-Type": "application/json"
        },
      ),
    );
  }

  static Future<Response> postData(
      {
   required String url,
    Map<dynamic, dynamic>? data,
  }) async {
    print(data);
    print(url);
    var result = dio!.post(url, data: data);
    print('response' + result.toString());
    return result;
  }
  static Future<Response> postDataList({
    required String url,
    List<Map<String, dynamic>>? data,
  }) async {
    return dio!.post(url, data: data);
  }



  static Future<Response> getData({
    required String url,
  }) async {
    return await dio!.get(
      url,
    );
  }

// this method to get some data and finish

}
