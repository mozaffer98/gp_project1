import 'package:flutter/material.dart';

class MyButton extends StatelessWidget {
  final title;
  final function;

  final double heigth ;

  const MyButton({ required this.function,required this.title , required this.heigth}) ;

  @override
  Widget build(BuildContext context) {
    return FlatButton(
        color: Colors.indigo,
        height: heigth,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
        onPressed: function,
        child: Text("$title" , style: TextStyle(color: Colors.white , fontSize: 30),));
  }
}
