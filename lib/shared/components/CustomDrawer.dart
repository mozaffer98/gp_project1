import 'package:flutter/material.dart';
import 'package:gp/draweerScreens/onBordingScreen.dart';
import 'package:gp/models/User.dart';
import 'package:gp/modules/DisableRequest/DisableRequest.dart';
import 'package:gp/styels/constants.dart';

class DrawerItem {
  String title;
  IconData icon ;

  DrawerItem(this.title, this.icon, {  Color });
}

class DrawerActivity extends StatefulWidget {
  final _draweItems = [
        DrawerItem("ASked help  ", Icons.accessibility_sharp,Color:Colors.black),
    DrawerItem("About Us", Icons.info,Color:Colors.black),
     DrawerItem("Ratting ", Icons.star,Color:Colors.black),
    DrawerItem("Logout", Icons.assignment_return_sharp,Color:Colors.black)

  ];

  @override
  State<StatefulWidget> createState() {
    return new DrawerActivityState();
  }
}

class DrawerActivityState extends State<DrawerActivity> {
  int _selectedIndex = 0;

//  String picsUrl =


  _getDrawerItemWidget(int pos) {
    switch (pos) {
      case 0:
        return onBordingScreen  ();
      case 1:
        return DisableRequest(new User());
      default:
        return null;
    }
  }

  _onSelectItem(int index) {
    setState(() => _selectedIndex = index);

    Navigator.of(context).pop(); // close the drawer
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> drawerOpts = [];
    for (var i = 0; i < widget._draweItems.length; i++) {
      var d = widget._draweItems[i];
      drawerOpts.add(new ListTile(
        leading:  Icon(d.icon),
        title: Text(d.title),
        selected: i == _selectedIndex,
        onTap: () => _onSelectItem(i),
      ));
    }

    return Scaffold(
      appBar: new AppBar(
        backgroundColor: mainColor,
        title: new Text(widget._draweItems[_selectedIndex].title),
      ),
      drawer:  Drawer(
        child: Column(
          children: <Widget>[
            UserAccountsDrawerHeader(
              decoration: BoxDecoration(
                color: Colors.indigo,
              ),
              accountEmail:  Text("email@gmail.com", style: TextStyle(fontSize: 18),),
              accountName:  Text("Your Name", style: TextStyle(fontSize: 16),),
              currentAccountPicture: new GestureDetector(
                child: new CircleAvatar(
                //  backgroundImage:  NetworkImage(),
                ),
                onTap: () => print("This is your current account."),
              ),
            ),
         Column(children: drawerOpts)
          ],
        ),
      ),
      body: _getDrawerItemWidget(_selectedIndex),
    );
  }
}

