import 'package:dio/dio.dart';
class AuthInterceptor extends InterceptorsWrapper {
  @override
  void onRequest(RequestOptions options, RequestInterceptorHandler handler) async {
      print("Request url : " + options.uri.toString() + " , data is : " + options.data.toString() + " , headers : " + options.headers.toString());
      return super.onRequest(options,handler);
  }

  @override
  void onResponse(Response response, ResponseInterceptorHandler handler) {
       print("Response is : " + response.data.toString() + " , headers : " + response.headers.toString());
       return super.onResponse(response, handler);
  }
}

